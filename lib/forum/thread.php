<?php


class forum_thread {
	public $id;
	public $title;
	public $closed;
	public $pinned;
	public $last_post;
	public $count_posts;
	public $create_date;
	public $create_by;
	public $update_date;
	public $update_by;

	/** @var forum_board */
	public $board;

	public static function create($id, forum_board $board) {
		$result = new self();
		$result->id = $id;
		$result->board = $board;
		return $result;
	}

	/**
	 * @return forum_board
	 */
	public function loadBoard() {
		return $this->board = db()->forum_board->row( $this->board )->object( 'forum_board' );
	}

	public function move(forum_board $destination) {
		db()->forum_thread->updateRow(array('board' => $destination->id), $this->id);
		$destination->update();
		$this->board->update();
	}

	public function close($closed=1) {
		db()->forum_thread->updateRow(array('closed' => $closed), $this->id);
	}

	public function delete() {
		db()->forum_thread->delRow($this->id);
		$this->board->update();
	}

	public function rename($name) {
		db()->forum_thread->updateRow(array('title' => $name), $this->id);
	}

	public function markRead() {
		if( !$user = iv::get('user')) return;

		if( $this->last_post < $user->last_read ) {
			db()->forum_unread->del("thread = %d AND create_by = %d", $this->id, $user->id );
		} else {
			db()->user_data->updateRow(array('last_read' => $this->last_post), $user->id);

			db()->query("
				REPLACE INTO forum_unread (thread, create_by, create_date)
				SELECT id, %d, %d FROM forum_thread
				WHERE last_post > %d AND last_post < %d",
					$user->id, time(), $user->last_read, $this->last_post );
		}
	}

	public function reply($title, $text){
		db()->forum_post->insert(array(
			'thread' => $this->id,
			'title' => $title,
			'text_raw' => $text
		));

		$this->update();
	}

	public function update() {
		$data = db()->query("SELECT MAX(id) AS last_post, count(*) AS count_posts FROM forum_post WHERE thread = %d", $this->id )->assoc();
		db()->forum_thread->updateRow($data, $this->id);
		$this->board->update();
	}

	/**
	 * @param int $id
	 * @return forum_thread
	 * @throws Exception
	 */
	public static function load($id) {
		if( !$thread = db()->forum_thread->row( $id )->object( 'forum_thread' ))
			throw new Exception('Thread nicht gefunden!');
		return $thread;
	}
} 