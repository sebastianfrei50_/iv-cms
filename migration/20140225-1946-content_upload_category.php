<?php

function install() {
	db()->query("CREATE TABLE `content_upload_category` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `name` varchar(200) NOT NULL,
	  `created_by` int(10) unsigned DEFAULT NULL,
	  `created_date` int(10) unsigned DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
}

function remove() {
	db()->query("DROP TABLE `content_upload_category`");
}
